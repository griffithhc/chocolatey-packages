$manufacturer   = 'Dell'
$packageName    = $manufacturer + "Driver Package"
$majorversion   = '2018'
$minorversion   = '7.19'

#Force TLS1.2
add-type @"
using System.Net;
using System.Security.Cryptography.X509Certificates;
public class TrustAllCertsPolicy : ICertificatePolicy {
    public bool CheckValidationResult(
        ServicePoint srvPoint, X509Certificate certificate,
        WebRequest request, int certificateProblem) {
        return true;
    }
}
"@
$AllProtocols = [System.Net.SecurityProtocolType]'Tls12'
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

#Set the Deployment Tools path variable
$deploymentToolsPath = Join-Path $Env:SystemDrive "\VCU-Deploy"

#Download/Import driver info spreadsheet
$driverBasePath = Join-Path $deploymentToolsPath "\drivers\$manufacturer"
$driverCSVFile  = Join-Path $driverBasePath "\$manufacturer-drivers.csv"
$driverCSVURL   = "https://docs.google.com/spreadsheets/d/e/2PACX-1vQeIMlUmfxyFUd8XV2SWqWq1OuGOagD4gVsYqj3nbzhLShBATp_chDaEdDnPLMf5dGrF_h_5S-eOfiW/pub?gid=0&single=true&output=csv"

#Ensure Driver CSV download destiation directory exists and download the file
if (!(Test-Path $driverBasePath)) { New-Item -Type Directory -Path $driverBasePath -Force }
Invoke-WebRequest -URI $driverCSVURL -outfile $driverCSVFile

#Import CSV file into array
$driverInfo     = Import-CSV -Path $driverCSVFile

#Determine Model Information
$modelTitle     = (Get-WmiObject -Class Win32_ComputerSystem).Model
$modelNumber    = $modelTitle.Split(' ')[-1]

#Loop through array to match and select driver package according to "model family", i.e. Latitude E6, Optiplex D9, etc
foreach($driverItem in $driverInfo){

    if($modelNumber -match $driverItem.ModelNumber){
       
        #Set some variables to be able to download the appropriate driver pacakge
        $manufacturer      = $driverItem.Manufacturer
        $modelFamily       = $driverItem.ModelFamily
        $workstationSeries = $driverItem.workstationSeries
        $packageURL        = $driverItem.URL
        $packageChecksum   = $driverItem.Checksum
    }
}

#Workstation Info Output
Write-Host "Model Number       : " $modelTitle -ForegroundColor Magenta -BackgroundColor DarkMagenta
Write-Host "Model Family       : " $modelFamily -ForegroundColor Magenta -BackgroundColor DarkMagenta
Write-Host "Workstation Series : " $workstationSeries -ForegroundColor Magenta -BackgroundColor DarkMagenta
Write-Host "Package URL        : " $packageURL -ForegroundColor Magenta -BackgroundColor DarkMagenta
Write-Host "Package Checksum   : " $packageChecksum -ForegroundColor Magenta -BackgroundColor DarkMagenta

#Set some arguments according to variables set above for downloading the appropriate driver package
$driverFolderName   = $modelFamily + $workstationSeries
$driverPackageDest  = Join-Path $driverBasePath $driverFolderName
$zipArgs = @{
  packageName   = $packageName
  unzipLocation = $driverPackageDest
  url           = $packageURL
  checksum      = $packageChecksum
  checksumType  = 'sha1'
}

#Download, verify hash, and unzip application zip package
Install-ChocolateyZipPackage @zipArgs
Start-Sleep -s 3

#Import drivers
$driverFolderName   = $modelFamily + $workstationSeries
$driverFullPath     = Join-Path $driverBasePath $driverFolderName
Get-ChildItem -Path $driverFullPath -Recurse | Where-Object -Property Extension -EQ ".inf" | ForEach { PnPUtil.exe -a $PSItem.FullName } 

Write-Host "Driver Installation Complete!" -ForegroundColor Magenta -BackgroundColor DarkMagenta



